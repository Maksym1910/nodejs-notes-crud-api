'use strict';

const express = require('express');
const router = new express.Router();

const {registration, login} = require('../services/authService');
const {asyncWrapper} = require('../utils/apiUtils');
const {InvalidRequestError} = require('../utils/errors');
// const {
//   registrationValidator
// } = require('../middlewares/validationMiddleware');

router.post(
    '/register',
    asyncWrapper(async (request, response) => {
      const {username, password} = request.body;

      if (!username || !password) {
        throw new InvalidRequestError('Invalid username or password');
      }

      await registration({username, password});
      response.json({message: 'Success'});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (request, response) => {
      const {username, password} = request.body;

      const jwtToken = await login({username, password});
      response.json({
        message: 'Success',
        jwt_token: jwtToken,
      });
    }),
);

module.exports = {
  authRoute: router,
};
