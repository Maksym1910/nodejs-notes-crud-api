'use strict';

const express = require('express');
const router = new express.Router();

const {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteTextByIdForUser,
  updateNoteCheckByIdForUser,
  deleteNoteByIdForUser,
} = require('../services/noteService');

const {asyncWrapper} = require('../utils/apiUtils');
const {InvalidRequestError} = require('../utils/errors');
// const {noteTextValidation} = require('../middlewares/validationMiddleware');

router.get(
    '/',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const offset = request.query.offset;
      const limit = request.query.limit;

      const notes = await getNotesByUserId(userId, offset, limit);

      response.json(notes);
    }),
);

router.post(
    '/',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;

      await addNoteToUser(userId, request.body);

      response.json({message: 'Success'});
    }),
);

router.get(
    '/:id',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const {id} = request.params;

      const note = await getNoteByIdForUser(id, userId);

      response.json(note);
    }),
);

router.put(
    '/:id',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const {id} = request.params;
      const {text} = request.body;

      const updated = await updateNoteTextByIdForUser(id, userId, text);

      if (!updated) {
        throw new InvalidRequestError('No note with such id found');
      }

      response.json({message: 'Success'});
    }),
);

router.patch(
    '/:id',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const {id} = request.params;

      await updateNoteCheckByIdForUser(id, userId);

      response.json({message: 'Success'});
    }),
);

router.delete(
    '/:id',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const {id} = request.params;

      const deleted = await deleteNoteByIdForUser(id, userId);

      if (!deleted) {
        throw new InvalidRequestError('No note with such id found');
      }

      response.json({message: 'Success'});
    }),
);

module.exports = {
  notesRoute: router,
};
