'use strict';

const express = require('express');
const router = new express.Router();

const {
  getUserProfileInfo,
  deleteUser,
  changePassword,
} = require('../services/userService');

const {asyncWrapper} = require('../utils/apiUtils');
// const {
//   changePasswordValidation,
// } = require('../middlewares/validationMiddleware');

router.get(
    '/me',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const userProfileInfo = await getUserProfileInfo(userId);
      response.json(userProfileInfo);
    }),
);

router.delete(
    '/me',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;

      await deleteUser(userId);

      response.json({message: 'Success'});
    }),
);

router.patch(
    '/me',
    asyncWrapper(async (request, response) => {
      const {userId} = request.user;
      const oldPassword = request.body.oldPassword;
      const newPassword = request.body.newPassword;

      await changePassword(userId, oldPassword, newPassword);

      response.json({message: 'Success'});
    }),
);

module.exports = {
  usersRoute: router,
};
