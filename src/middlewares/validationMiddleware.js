'use strict';

const Joi = require('joi');
const {ValidationError} = require('../utils/errors');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(5)
        .max(15)
        .required(),
    password: Joi.string()
        .min(8)
        .max(15)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new ValidationError(err.message));
  }
};

const changePasswordValidation = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .min(8)
        .max(15)
        .required(),
    newPassword: Joi.string()
        .min(8)
        .max(15)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new ValidationError(err.message));
  }
};

const noteTextValidation = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .min(1)
        .max(30)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new ValidationError(err.message));
  }
};

module.exports = {
  registrationValidator,
  changePasswordValidation,
  noteTextValidation,
};
