'use strict';

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
// const {InvalidCredentialsError} = require('../utils/errors');
const {InvalidRequestError} = require('../utils/errors');

const registration = async ({username, password}) => {
  const existedUser = await User.findOne({username});

  if (existedUser) {
    throw new InvalidRequestError(
        `User with ${username} username already exists`,
    );
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

const login = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    throw new InvalidRequestError('Invalid username or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidRequestError('Invalid username or password');
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, process.env.JWT_SECRET);
  return token;
};

module.exports = {
  registration,
  login,
};
