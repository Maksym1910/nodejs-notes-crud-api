'use strict';

const {Note} = require('../models/noteModel');
const {InvalidRequestError} = require('../utils/errors');

const getNotesByUserId = async (userId, offset = 0, limit = 0) => {
  const notes = await Note.find({userId})
      .skip(Number(offset))
      .limit(Number(limit));
  const count = await Note.count({userId});
  return {offset, limit, count, notes};
};

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
};

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  return {
    note: {
      _id: note._id,
      userId: note.userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdDate,
    },
  };
};

const updateNoteTextByIdForUser = async (noteId, userId, text) => {
  return await Note.findOneAndUpdate({_id: noteId, userId}, {$set: {text}});
};

const updateNoteCheckByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});

  if (!note) {
    throw new InvalidRequestError('No note with such id found');
  }

  note.completed = !note.completed;
  await note.save();
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  return await Note.findOneAndRemove({_id: noteId, userId});
};

module.exports = {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteTextByIdForUser,
  updateNoteCheckByIdForUser,
  deleteNoteByIdForUser,
};
