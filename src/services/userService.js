'use strict';

const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');
const {InvalidCredentialsError} = require('../utils/errors');

const getUserProfileInfo = async (userId) => {
  const user = await User.findById(userId);
  return {
    user: {
      _id: user._id,
      username: user.username,
      createdDate: user.createdAt,
    },
  };
};

const deleteUser = async (userId) => {
  await User.findOneAndRemove({_id: userId});
  await Note.deleteMany({userId});
};

const changePassword = async (userId, oldPassword, newPassword) => {
  const user = await User.findById(userId);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new InvalidCredentialsError('Invalid old password');
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await User.findByIdAndUpdate(userId, user);
};

module.exports = {
  getUserProfileInfo,
  deleteUser,
  changePassword,
};
