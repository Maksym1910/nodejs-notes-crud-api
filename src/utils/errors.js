/* eslint-disable require-jsdoc */
'use strict';

class NotesCrudApiError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRequestError extends NotesCrudApiError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

class InvalidCredentialsError extends NotesCrudApiError {
  constructor(message = 'Invalid credentials') {
    super(message);
    this.status = 401;
  }
}

class ValidationError extends NotesCrudApiError {
  constructor(message = 'Invalid data') {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  NotesCrudApiError,
  InvalidRequestError,
  InvalidCredentialsError,
  ValidationError,
};
